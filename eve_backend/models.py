from eve_backend import db

class ParentCategory(db.Model):
    __tablename__ = "parent_category"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    word_categories = db.relationship(
        "WordCategory", backref="parent_category", lazy=True
    )

    def __repr__(self):
        return f"{self.name}"

class WordCategory(db.Model):
    __tablename__ = "word_category"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    free = db.Column(db.Boolean, nullable=False)
    parent_category_id = db.Column(db.Integer, db.ForeignKey("parent_category.id"))
    words = db.relationship("Word", backref="word_category", lazy=True)

    def number_of_groups(self):
        return max(word.group for word in self.words)

    def __repr__(self):
        return f"{self.name}"

class Word(db.Model):
    __tablename__ = "word"
    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.String(64), nullable=False)
    word_category_id = db.Column(db.Integer, db.ForeignKey("word_category.id"))
    group = db.Column(db.Integer, nullable=False)
    definitions = db.relationship("Definition", backref="word", lazy=True)
    exercises = db.relationship("Exercise", backref="word", lazy=True)

    def __repr__(self):
        return f"{self.word} in group {self.group}"

class Definition(db.Model):
    __tablename__ = "definition"
    id = db.Column(db.Integer, primary_key=True)
    definition = db.Column(db.String(), nullable=False)
    source = db.Column(db.String(64), nullable=False)
    word_id = db.Column(db.Integer, db.ForeignKey("word.id"))

    def __repr__(self):
        return f"{self.definition} for {self.word}"

class Exercise(db.Model):
    __tablename__ = "exercise"
    id = db.Column(db.Integer, primary_key=True)
    sentence = db.Column(db.String(), nullable=False)
    answer = db.Column(db.String(64), nullable=False)
    word_id = db.Column(db.Integer, db.ForeignKey("word.id"))
    word_category_id = db.Column(db.Integer, db.ForeignKey("word_category.id"))
    group = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f"{self.sentence} with answer {self.answer} for word {self.word}"
